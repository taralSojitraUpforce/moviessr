import { Inter } from "@next/font/google";
import { FlexWrapper } from "../lib/config/style";
import Link from "next/link";
import CardContainer from "../lib/components/module/card/MediaCardContainer";
import { useGetTvListQuery } from "../lib/store/API/tv";
import LoaderWithBackdrop from "../lib/components/common/Loader/LoaderWithBackdrop";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { data, error, isLoading } = useGetTvListQuery();

  return (
    <>
      {error ? (
        <>Oh no, there was an error</>
      ) : isLoading ? (
        <>
          <LoaderWithBackdrop />
        </>
      ) : data && data.results ? (
        <>
          <FlexWrapper>
            {data.results.map((obj) => (
              <Link key={obj.id} href={`/tv/${obj.id}`}>
                <CardContainer
                  posterImg={obj.poster_path}
                  title={obj.name}
                  date={obj.first_air_date}
                  id={obj.id}
                />
              </Link>
            ))}
          </FlexWrapper>
        </>
      ) : null}
    </>
  );
}
