import Link from "next/link";
import React from "react";
import LoaderWithBackdrop from "../../lib/components/common/Loader/LoaderWithBackdrop";
import CardContainer from "../../lib/components/module/card/MediaCardContainer";
import { FlexWrapper } from "../../lib/config/style";
import { useGetTvListQuery } from "../../lib/store/API/tv";

const tv = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { data, error, isLoading } = useGetTvListQuery();

  return (
    <>
      {error ? (
        <>Oh no, there was an error</>
      ) : isLoading ? (
        <>
          <LoaderWithBackdrop />
        </>
      ) : data && data.results ? (
        <>
          <FlexWrapper>
            {data.results.map((obj) => (
              <Link key={obj.id} href={`/tv/${obj.id}`}>
                <CardContainer
                  posterImg={obj.poster_path}
                  title={obj.name}
                  date={obj.first_air_date}
                  id={obj.id}
                />
              </Link>
            ))}
          </FlexWrapper>
        </>
      ) : null}
    </>
  );
};

export default tv;
