import React from "react";
import DetailsContainer from "../../lib/components/module/details/DetailsContainer";
import { API_KEY, BASE_URL } from "../../lib/config";
import { ApiDetails } from "../../lib/types/Api";

const stvId = ({ data }: { data: ApiDetails }) => {
  return (
    <div>
      <DetailsContainer
        backdropPath={data.backdrop_path}
        posterPath={data.poster_path}
        name={data.name}
        overview={data.overview}
      />
    </div>
  );
};

export default stvId;

export async function getServerSideProps(context: any) {
  const tvId = context.params?.tvId;

  const data = await fetch(
    `${BASE_URL}/tv/${tvId}?api_key=${API_KEY}&language=en-US`
  )
    .then((response) => response.json())
    .then((data) => data);

  return {
    props: { data }, // will be passed to the page component as props
  };
}
