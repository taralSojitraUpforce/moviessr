import React from "react";
import DetailsContainer from "../../lib/components/module/details/DetailsContainer";
import { API_KEY, BASE_URL } from "../../lib/config";
import { ApiDetails } from "../../lib/types/Api";

const movieId = ({ data }: { data: ApiDetails }) => {
  return (
    <div>
      <DetailsContainer
        backdropPath={data.backdrop_path}
        posterPath={data.poster_path}
        name={data.title}
        overview={data.overview}
      />
    </div>
  );
};

export default movieId;

export async function getServerSideProps(context: any) {
  const movieId = context.params?.movieId;

  const data = await fetch(
    `${BASE_URL}/movie/${movieId}?api_key=${API_KEY}&language=en-US`
  )
    .then((response) => response.json())
    .then((data) => data);

  return {
    props: { data }, // will be passed to the page component as props
  };
}

// export const getServerSideProps = wrapper.getServerSideProps(
//   (store) => async (context) => {
//     const id = context.params?.movieId;

//     if (typeof id === "string") {
//       //   store.dispatch(getMovieDetails.initiate(id));
//       store.dispatch({ type: "TICK", payload: "was set in other page" });
//     }
//     // const data =
//     //   (await typeof movieId) === "string" ? useGetMovieDetailsQuery(id) : "asd";

//     const data = await fetch(
//       `${BASE_URL}/movie/${id}?api_key=${API_KEY}&language=en-US`
//     )
//       .then((response) => response.json())
//       .then((data) => data);
//     // const datas = await getMovieDetails.initiate(id);
//     // // const datasd = datas();
//     // console.log("data is", datas);
//     return {
//       props: { data },
//     };
//   }
// );
