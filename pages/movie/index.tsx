import Link from "next/link";
import React from "react";
import LoaderWithBackdrop from "../../lib/components/common/Loader/LoaderWithBackdrop";
import CardContainer from "../../lib/components/module/card/MediaCardContainer";
import { FlexWrapper } from "../../lib/config/style";
import { useGetMovieListQuery } from "../../lib/store/API/movie";

const movie = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { data, error, isLoading } = useGetMovieListQuery();

  return (
    <>
      {error ? (
        <>Oh no, there was an error</>
      ) : isLoading ? (
        <>
          <LoaderWithBackdrop />
        </>
      ) : data && data.results ? (
        <>
          <FlexWrapper>
            {data.results.map((obj) => (
              <Link key={obj.id} href={`/movie/${obj.id}`}>
                <CardContainer
                  posterImg={obj.poster_path}
                  title={obj.title}
                  date={obj.release_date}
                  id={obj.id}
                />
              </Link>
            ))}
          </FlexWrapper>
        </>
      ) : null}
    </>
  );
};

export default movie;
