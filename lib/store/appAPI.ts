import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { HYDRATE } from "next-redux-wrapper";
import { BASE_URL } from "../config";

const baseQuery = fetchBaseQuery({
  baseUrl: BASE_URL,
});

// initialize an empty api service that we'll inject endpoints into later as needed
export const appAPI = createApi({
  reducerPath: "appAPI",
  baseQuery: baseQuery,
  extractRehydrationInfo(action, { reducerPath }) {
    if (action.type === HYDRATE) {
      return action.payload[reducerPath];
    }
  },
  tagTypes: ["movie", "tv"],
  endpoints: () => ({}),
});
