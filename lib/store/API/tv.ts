import { API_KEY } from "../../config";
import { ApiObject } from "../../types/Api";
import { appAPI } from "../appAPI";

const movieAPI = appAPI.injectEndpoints({
  endpoints: (builder) => ({
    getTvList: builder.query<{ results: Array<ApiObject> }, void>({
      query: () => {
        return {
          url: `/tv/popular?api_key=${API_KEY}&language=en-US&page=1`,
          method: "GET",
        };
      },
      providesTags: ["tv"],
    }),
    getTvDetails: builder.query({
      query: (id) => {
        return {
          url: `/tv/${id}?api_key=${API_KEY}&language=en-US`,
          method: "GET",
        };
      },
      providesTags: ["tv"],
    }),
  }),
});

export const { useGetTvListQuery } = movieAPI;

export const { getTvList } = movieAPI.endpoints;
