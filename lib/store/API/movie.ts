import { API_KEY } from "../../config";
import { ApiObject } from "../../types/Api";
import { appAPI } from "../appAPI";

const movieAPI = appAPI.injectEndpoints({
  endpoints: (builder) => ({
    getMovieList: builder.query<{ results: Array<ApiObject> }, void>({
      query: () => {
        return {
          url: `/movie/popular?api_key=${API_KEY}&language=en-US&page=1`,
          method: "GET",
        };
      },
      providesTags: ["movie"],
    }),
    getMovieDetails: builder.query({
      query: (id) => {
        return {
          url: `/movie/${id}?api_key=${API_KEY}&language=en-US`,
          method: "GET",
        };
      },
      providesTags: ["movie"],
    }),
  }),
});

export const { useGetMovieListQuery, useGetMovieDetailsQuery } = movieAPI;

export const { getMovieList, getMovieDetails } = movieAPI.endpoints;
