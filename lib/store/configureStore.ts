import { configureStore } from "@reduxjs/toolkit";
import { createWrapper } from "next-redux-wrapper";
import { appAPI } from "./appAPI";

export const makeStore = () =>
  configureStore({
    reducer: {
      [appAPI.reducerPath]: appAPI.reducer,
    },
    middleware: (gDM) =>
      gDM({ immutableCheck: false, serializableCheck: false }).concat(
        appAPI.middleware
      ),
  });

export type AppStore = ReturnType<typeof makeStore>;
export type RootState = ReturnType<AppStore["getState"]>;
export type AppDispatch = AppStore["dispatch"];

export const wrapper = createWrapper<AppStore>(makeStore, { debug: true });
