import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
  backDrop: {
    color: theme.palette.common.white,
    zIndex: theme.zIndex.drawer + 1000,
  },
  circularLoader: (props) => ({
    "&.MuiCircularProgress-root": {
      color: props.loaderColor,
    },
  }),
}));

export default useStyles;
