import * as React from "react";
// Material UI
import Backdrop from "@mui/material/Backdrop";
//Internal
import CircularLoader from "./CircularLoader";

const LoaderWithBackdrop = () => {
  return (
    <Backdrop open onClick={(e) => e.stopPropagation()}>
      <CircularLoader />
    </Backdrop>
  );
};

export default LoaderWithBackdrop;
