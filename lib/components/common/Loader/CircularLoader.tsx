import React from "react";

// Material UI
import { CircularProgress } from "@mui/material";

const CircularLoader = () => {
  return <CircularProgress />;
};

export default CircularLoader;
