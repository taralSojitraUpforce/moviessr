import React from "react";
import Details from "./Details";

const DetailsContainer = ({
  backdropPath,
  posterPath,
  name,
  overview,
}: {
  backdropPath: string;
  posterPath: string;
  name: string | undefined;
  overview: string;
}) => {
  return (
    <Details
      backdropPath={backdropPath}
      posterPath={posterPath}
      name={name}
      overview={overview}
    />
  );
};

export default DetailsContainer;
