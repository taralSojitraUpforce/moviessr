import { styled } from "@mui/system";

export const ImageWrapper = styled("div")({
  display: "flex",
  alignItems: "center",
  height: "100%",
  width: "100%",
  padding: 30,
  backgroundImage: `linear-gradient(to right, rgba(52.5, 52.5, 94.5, 1) calc((50vw - 170px) - 340px), rgba(52.5, 52.5, 94.5, 0.84) 30%, rgba(52.5, 52.5, 94.5, 0.84) 100%)`,
});

export const PosterContainer = styled("div")({
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
  backgroundPosition: "50% 50%",
  width: "100%",
  height: "60vh",
  position: "relative",
  borderBottom: "1px solid #35355f",
  display: "flex",
  alignItems: "center",
});
