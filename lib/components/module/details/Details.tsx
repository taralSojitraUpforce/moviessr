import { Box, Grid, Typography } from "@mui/material";
import Image from "next/image";
import React from "react";
import { BASE_IMAGE_URL } from "../../../config";
import { ImageWrapper, PosterContainer } from "./style";

const Details = ({
  backdropPath,
  posterPath,
  name,
  overview,
}: {
  backdropPath: string;
  posterPath: string;
  name: string | undefined;
  overview: string;
}) => {
  return (
    <>
      <Grid>
        <PosterContainer
          sx={{ backgroundImage: `url(${BASE_IMAGE_URL}${backdropPath})` }}
        >
          <ImageWrapper>
            <Image
              height={300}
              width={200}
              src={`${BASE_IMAGE_URL}${posterPath}`}
              alt=""
            />
          </ImageWrapper>
        </PosterContainer>

        <Box sx={{ padding: "20px 60px" }}>
          <Typography variant="h2" component="h2">
            {name}
          </Typography>
          <Box>
            <Typography variant="h4" component="h3">
              Overview
            </Typography>
            <Typography variant="body1" component="p">
              {overview}
            </Typography>
          </Box>
        </Box>
      </Grid>
    </>
  );
};

export default Details;
