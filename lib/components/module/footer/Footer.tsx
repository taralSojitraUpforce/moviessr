import { Typography } from "@mui/material";
import React from "react";
import { FooterWrapper } from "./style";

const Footer = () => {
  return (
    <FooterWrapper>
      <Typography variant="body2">
        Copyright © 2023 <strong> Upforce Tech </strong> All rights reserved.
      </Typography>
    </FooterWrapper>
  );
};

export default Footer;
