import { styled } from "@mui/system";

export const FooterWrapper = styled("div")({
  backgroundColor: "#1876d2",
  display: "flex",
  justifyContent: "center",
  alignContent: "center",
});
