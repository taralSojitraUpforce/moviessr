import { styled } from "@mui/system";

export const MainWrapper = styled("div")({
  width: "100%",
  display: "flex",
  justifyContent: "space-between",
  flexWrap: "wrap",
  //   backgroundColor: "white",
});

export const PosterContainer = styled("div")({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});
