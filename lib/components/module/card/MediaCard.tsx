import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import Image from "next/image";
import { BASE_IMAGE_URL } from "../../../config";
import { MainWrapper, PosterContainer } from "./style";

export default function MediaCard({
  posterImg,
  title,
  date,
  id,
}: {
  posterImg: string | undefined;
  title: string | undefined;
  date: string | undefined;
  id: number | undefined;
}) {
  return (
    <MainWrapper>
      <CardActionArea
        sx={{
          width: "calc((100vw) / 6)",
          minHeight: 350,
          minWidth: 200,
        }}
      >
        <PosterContainer>
          <Image
            width={200}
            height={300}
            src={`${BASE_IMAGE_URL}${posterImg}`}
            alt="green iguana"
          />
        </PosterContainer>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {title}
          </Typography>
          <Typography variant="body2">{date}</Typography>
        </CardContent>
      </CardActionArea>
    </MainWrapper>
  );
}
