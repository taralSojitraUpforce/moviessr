import React from "react";
import MediaCard from "./MediaCard";

const CardContainer = ({
  posterImg,
  title,
  date,
  id,
}: {
  posterImg: string | undefined;
  title: string | undefined;
  date: string | undefined;
  id: number | undefined;
}) => {
  return (
    <>
      <MediaCard posterImg={posterImg} title={title} date={date} id={id} />
    </>
  );
};

export default CardContainer;
