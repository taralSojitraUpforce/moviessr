import { styled } from "@mui/system";

export const BodyWrapper = styled("div")({
  height: "100vh",
  width: "100vw",
});

export const ChildrenWrapper = styled("div")({
  height: "calc(100vh - 90px)",
  overflow: "scroll",
});
