import type { ReactElement } from "react";
import Footer from "../components/module/footer/Footer";
import HeaderContainer from "../components/module/header/HeaderContainer";
import { BodyWrapper, ChildrenWrapper } from "./style";

const Wrapper = ({ children }: any) => {
  return (
    <>
      <BodyWrapper>
        <HeaderContainer />
        <ChildrenWrapper>{children}</ChildrenWrapper>
        <Footer />
      </BodyWrapper>
    </>
  );
};

export default Wrapper;
