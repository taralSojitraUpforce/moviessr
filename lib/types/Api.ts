export interface ApiObject {
  backdrop_path: string;
  genre_ids: Array<number>;
  id: number;
  original_language: string;
  overview: string;
  popularity: number;
  poster_path: string;
  vote_average: number;
  adult?: boolean;
  original_title?: string;
  release_date?: string;
  title?: string;
  video?: boolean;
  vote_count?: number;

  first_air_date?: string;
  name?: string;
  origin_country?: Array<string>;
  original_name?: string;
}

export interface ApiDetails {
  adult: boolean;
  backdrop_path: string;
  budget: number;
  homepage: string;
  id: number;
  imdb_id: string;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  created_by?: Array<{
    id: number;
    credit_id: string;
    name: string;
    gender: number;
    profile_path: string;
  }>;
  episode_run_time?: Array<string | number>;
  first_air_date?: string;
  genres: Array<{
    id: number;
    name: string;
  }>;
  in_production?: true;
  languages?: Array<string>;
  last_air_date?: string;
  last_episode_to_air?: {
    air_date: string;
    episode_number: number;
    id: number;
    name: string;
    overview: string;
    production_code: string;
    runtime: number;
    season_number: number;
    show_id: number;
    still_path: string;
    vote_average: number;
    vote_count: number;
  };
  name?: string;
  next_episode_to_air?: null;
  networks?: Array<{
    id: number;
    name: string;
    logo_path: string;
    origin_country: string;
  }>;
  number_of_episodes?: number;
  number_of_seasons?: number;
  origin_country?: Array<string>;
  original_name?: string;
  production_companies: Array<{
    id: number;
    name: string;
    logo_path: string;
    origin_country: string;
  }>;
  production_countries: Array<{
    iso_3166_1: string;
    name: string;
  }>;
  seasons?: Array<{
    air_date: string;
    episode_count: number;
    id: number;
    name: string;
    overview: string;
    poster_path: string;
    season_number: number;
  }>;
  spoken_languages: Array<{
    english_name: string;
    iso_639_1: string;
    name: string;
  }>;
  status: string;
  tagline: string;
  type?: string;
  vote_average: number;
  vote_count: number;

  belongs_to_collection?: {
    id: number;
    name: string;
    poster_path: string;
    backdrop_path: string;
  };
  release_date?: string;
  revenue?: number;
  runtime?: number;
  title?: string;
  video?: boolean;
}
