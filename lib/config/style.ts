import { styled } from "@mui/system";

export const FlexWrapper = styled("div")({
  display: "flex",
  //   alignItems: "center",
  justifyContent: "space-evenly",
  flexWrap: "wrap",
  paddingTop: 20,
});
